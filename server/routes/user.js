const express = require('express');
const router = express.Router();

var User = require('../models/user');
var middleWare = require('../routes/middleware/middleware');

//  User Sign up route
router.post('/', function (req, res) {
    // Create a new user 

    var newUser = new User({
        email: req.body.email,
        fullName: req.body.fullName,
        birthDate: req.body.birthDate,
        cityState: req.body.cityState,
        country: req.body.country,
        authType: req.body.authType,
        profilePicture: req.body.profilePicture || 'src/assets/images/default_user_avatar.png'
    });

    //console.log(newUser);

    // Check if a signing up user is admin
    if(req.body.adminCode === process.env.ADMIN_CODE) {
        newUser.isAdmin = true;
    }

    User.findOne({email : req.body.email}, function(err, user){
        // Check if error in finding user

       // console.log("Inside user post route "+user);


         if(err){        
             return res.status(500).json({
                 title: 'An error occured',
                 error: err
             });
         }

         // Check if user not found then save user
         if(user == null || user == undefined || user == "") {

        //    console.log("Inside save user post route "+user);
            // Save User
          return  User.create(newUser, function(err, user){
                if(err){
                //    console.log("Error while creating user");
                    return res.status(500).json({
                        title: 'An error occurred',
                        error: err
                    });
                }

                else {
                 //  console.log("Creating user");

                   return res.status(201).json({
                        message: 'User successfully created',
                        obj: user
                    });
                }
            
            });    

         }

       //  console.log("Inside user already exists "+user);
         return res.status(200).json({
                        message: 'User already exists',
                         email: user.email,
                         userId: user._id
                     }); 



    })



});

// router.post('/login', function(req, res, next) {
//     passport.authenticate("local", function(err, user, info) {
//         if(err){
//             return res.status(500).json({
//                 title: 'An error occurred',
//                 error: err
//             });
//         }
//         // Check if user not found
//         if(!user) {
//             return res.status(401).json({
//                 title: 'Login Failed',
//                 error: {message: 'Invalid Login Credentials'} 
//             });            
//         }
//         req.logIn(user, function(err) {
//             if (err) { 
//                 return res.status(500).json({
//                     title: 'An error occurred',
//                     error: err
//                 });
//             }
//             res.status(200).json({
//                 message: 'Successfully signed in',
//                 firstName: user.firstName,
//                 adminFlag: user.isAdmin,
//                 loginFlag: true
//             });
//         });
//     }) (req, res, next);
// });

// To find a user
router.get('/', function(req, res, next) {
    console.log("inside get user route");
    console.log(req.query.email);
    User.findOne({email : req.query.email}, function(err, user){
        // Check if error in finding user
         if(err){        
             return res.status(500).json({
                 title: 'An error occurred',
                 error: err
             });
         }
         // Check if user not found
         if(!user) {
             return res.status(401).json({
                 title: 'User Not Found',
                 error: {message: 'User Not Found'} 
             });            
         }
         console.log(user);

         return res.status(200).json({
            message: 'Successfully found user',
             email: user.email,
             fullName: user.fullName,
             birthDate: user.birthDate,
             cityState: user.cityState,
             country: user.country
         }); 
     
     })
    
});

// router.get('/logout', function(req, res, next) {
//     req.logOut();
//     // req.session.destroy(function() {
//     //     res.clearCookie('connect.sid');
//     // });
//     res.status(200).json({
//         message: 'Successfully logged out',
//     });


// });


module.exports = router; 