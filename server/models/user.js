var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
    fullName: { type: String, required: true },
    email: {
        type: String, required: true, unique: true, trim: true,
        match: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
    },
    

    birthDate: { type: Date, required: false },
    cityState: { type: String, required: false },

    country: { type: String, required: false },
    authType: { type: String, required:true}, // google auth or local auth
    profilePicture: { type: String, required: true, default: "src/assets/images/default_user_avatar.png"}, // google auth or local auth
    userProfile: {
       type: mongoose.Schema.Types.ObjectId,
       ref: "UserProfile"
    },
    isAdmin: {type: Boolean, default: false},
    suggestedUniversities: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "SuggestedUniversities"
     }

});


module.exports = mongoose.model('User', userSchema);