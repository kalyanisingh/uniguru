/*// Environment variables
require('dotenv').config()

const express = require('express');
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');
var mongoose = require('mongoose');
var stripe = require('stripe')(process.env.STRIPE_KEY);

// var User = require('./models/user');

var userRoutes = require('./routes/user');
// var fbRoutes = require('./routes/user-fb');
// var Routes = require('./routes/payment');
// var profileRoutes = require('./routes/profile');
// var fileUploadRoutes = require('./routes/file-upload');
// var adminRoutes = require('./routes/admin');

// var suggestedRoutes = require('./routes/sugg-uni');

const app = express();


// Connecting to remote server database - MongoDB Atlas
const atlasDBconnection = 'mongodb+srv://admin:'+process.env.ATLAS_DATABASE_PWD+'@unigurucluster.aonks.mongodb.net/uniguru?retryWrites=true&w=majority';
mongoose.connect(atlasDBconnection,  { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(express.static(path.join(__dirname, '../dist')));


// app.use('/user', userRoutes);
// app.use('/user/facebook', fbRoutes);
// app.use('/payment', paymentRoutes);
// app.use('/profile', profileRoutes);
// app.use('/file', fileUploadRoutes);
// app.use('/admin', adminRoutes);
//app.use('/suggested', suggestedRoutes);

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '../dist/index.html'));
});

const port = process.env.PORT || '3000';
app.set('port', port);

const server = http.createServer(app);

server.listen(port, () => {
    console.log(`Server listening on localhost:${port}`);
});

*/


// Environment variables
require('dotenv').config()
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');
var mongoose = require('mongoose');

const app = express();


// Connecting to remote server database - MongoDB Atlas
const atlasDBconnection = 'mongodb+srv://admin:'+process.env.ATLAS_DATABASE_PWD+'@unigurucluster.aonks.mongodb.net/uniguru?retryWrites=true&w=majority';
mongoose.connect(atlasDBconnection,  { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });


var User = require('./models/user');

var userRoutes = require('./routes/user');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(cors());

// serve static files from dist directory
app.use(express.static(path.join(__dirname, '../dist/uniguru')));


app.use('/user', userRoutes);


// redirect all calls to index.html
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, '../dist/uniguru/index.html'));
});

// server setup for listening
const port = process.env.PORT || '3000';
app.listen(port, () => {
    console.log(`Server listening on the port::${port}`);
});