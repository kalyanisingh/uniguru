import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ProfileComponent } from './profile/profile.component';
import { EvalComponent } from './eval/eval.component';
import { SuggestedComponent } from './suggested/suggested.component';
import { AccountInfoComponent } from './accountinfo/accountinfo.component';
import { ReactiveFormsModule } from '@angular/forms';

const DASHBOARD_ROUTES: Routes = [
    { path: '', redirectTo: '/home/dashboard/eval', pathMatch: 'full' },
    { path: 'profile', component: ProfileComponent },
    { path: 'accountinfo', component: AccountInfoComponent},
    { path: 'eval', component: EvalComponent },
    { path: 'suggested', component: SuggestedComponent}
];

@NgModule({
  imports: [ CommonModule,
             RouterModule.forChild(DASHBOARD_ROUTES),
             ReactiveFormsModule
           ],
  exports: [ RouterModule ]
})
export class DashboardRoutingModule {}