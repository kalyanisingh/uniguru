import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatExpansionModule } from '@angular/material/expansion'; 
import { MatSortModule } from '@angular/material/sort';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
// import { MatSidenavContent } from '@angular/material/sidenav';
import { MatSidenav } from '@angular/material/sidenav';
import { MatButtonModule } from '@angular/material/button'; 
import { MatIconModule } from '@angular/material/icon';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatStepperModule} from '@angular/material/stepper';
import {MatProgressBarModule} from '@angular/material/progress-bar'; 
import {MatTableModule} from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { DashboardComponent } from './dashboard.component';
import { ProfileComponent } from './profile/profile.component';
// import { SuiModule } from 'ng2-semantic-ui';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { EvalComponent } from './eval/eval.component';
// import { NgCircleProgressModule } from 'ng-circle-progress';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
// import { FileUploadModule } from 'ng2-file-upload';
import { AccountInfoComponent } from './accountinfo/accountinfo.component';
import { SuggestedComponent } from './suggested/suggested.component';
import { RouterModule, Routes } from '@angular/router';


const DASHBOARD_ROUTES: Routes = [
  { path: '', redirectTo: 'eval', pathMatch: 'full' },
  { path: 'eval', component: EvalComponent },
  { path: 'sugguni', component: SuggestedComponent },
  { path: 'credential', component: ProfileComponent }
];

@NgModule({
  imports: [
    CommonModule,
     RouterModule.forChild(DASHBOARD_ROUTES),
    // SuiModule,
    ReactiveFormsModule,
    DashboardRoutingModule,
    FormsModule,
    MatInputModule,
    MatExpansionModule,
    MatSelectModule,
    MatFormFieldModule,
    MatListModule,
    MatMenuModule,
    MatSidenavModule,
    // MatSidenavContent,
    // MatSidenav,
    MatButtonModule,
    MatIconModule,
    MatStepperModule,
    MatTableModule,
    MatProgressBarModule,
    MatCardModule,
    MatListModule,
    // Specify ng-circle-progress as an import
    // NgCircleProgressModule.forRoot({
    //   // set defaults here  
    //   radius: 100,
    //   outerStrokeWidth: 16,
    //   innerStrokeWidth: 8,
    //   outerStrokeColor: "#78C000",
    //   innerStrokeColor: "#C7E596",
    //   animationDuration: 300
    // }),
    MatExpansionModule,
    FlexLayoutModule,
    // FileUploadModule,
  ],

  exports: [
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatListModule,
    // MatSidenavModule
  ],
  //providers: [AdminService],

  declarations: [
    ProfileComponent,
    EvalComponent,
    SuggestedComponent,
    AccountInfoComponent    
  ]
})
export class DashboardModule { }

