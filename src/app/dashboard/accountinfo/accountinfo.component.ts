import { Component, OnInit} from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { GoogleAuthService } from 'src/app/services/auth/google-auth.service';
import { UserService } from 'src/app/services/user/user.service';


const uri = 'http://localhost:3000/file/upload';

@Component({
  selector: 'app-accountinfo',
  templateUrl: './accountinfo.component.html',
  styleUrls: ['./accountinfo.component.scss']
})


export class AccountInfoComponent implements OnInit {
  public currentUser: any = {};

  constructor(
    public googleAuthService: GoogleAuthService,
    private userService: UserService
              ) {} 
            

  ngOnInit() {

    // .subscribe(
      // currentUserData => {
      //   console.log(currentUserData)
      // } 
    // )
  


    this.googleAuthService.user.subscribe({
      next: currentUser => {
        this.currentUser = currentUser;  
        // console.log(this.currentUser.email);    
        this.userService.getCurrentUser(this.currentUser.email).subscribe(
          user => {
            this.currentUser = user;
            console.log(this.currentUser);
          }
        )
      }
    })

    console.log(this.currentUser);


    
  }  

}
