import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutusComponent } from './aboutus/aboutus.component';
import { AnimeComponent } from './anime/anime.component';
import { ContactusComponent } from './contactus/contactus.component';
import { GetstartedComponent } from './getstarted/getstarted.component';
import { PlansComponent } from './plans/plans.component';
import { MatCardModule } from '@angular/material/card';
import { HomeComponent } from './home.component';

@NgModule({
  declarations: [
    AboutusComponent, 
    AnimeComponent, 
    ContactusComponent, 
    GetstartedComponent, 
    PlansComponent,
    HomeComponent
  ],
  imports: [
    CommonModule,
    MatCardModule
  ]
})
export class HomeModule { }
