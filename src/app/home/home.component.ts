import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { HeaderService } from '../services/header/header.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  planClick: boolean = false;

  @ViewChild("plans") plans!: ElementRef;
  @ViewChild("home") home!: ElementRef;
  @ViewChild("aboutus") aboutus!: ElementRef;
  @ViewChild("contactus") contactus!: ElementRef;
  
  constructor(
    private headerService: HeaderService,
  ) { }

  scroll(element: HTMLElement) {
    element.scrollIntoView();
  }

  ngOnInit(): void {
    this.headerService.getPlanClicked().subscribe(
      isPlansClicked => {
        if(isPlansClicked === true){
          this.plans.nativeElement.scrollIntoView({behavior: 'smooth', block: "start"});
        } 
    });
    this.headerService.getHomeClick().subscribe(
      isHomeClicked => {
        if(isHomeClicked === true){
          this.home.nativeElement.scrollIntoView({behavior: 'smooth', block: "start"});
        } 
    });
    this.headerService.getAboutusClick().subscribe(
      isAboutusClicked => {
        if(isAboutusClicked === true){
          this.aboutus.nativeElement.scrollIntoView({behavior: 'smooth', block: "start"});
        } 
    });
    this.headerService.getContactusClick().subscribe(
      isContactusClicked => {
        if(isContactusClicked === true){
          this.contactus.nativeElement.scrollIntoView({behavior: 'smooth', block: "start"});
        } 
    });
    this.headerService.setHomeFlag(true);
  }
}


