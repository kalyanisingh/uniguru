import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthComponent } from './auth.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { AuthRoutingModule } from './auth-routing.module';
import { ConfirmedValidator } from './signup/customValidators.validator';
import { NG_VALIDATORS, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDividerModule } from '@angular/material/divider';
import { MatInputModule} from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faGoogle, faLinkedinIn, faLinkedin } from '@fortawesome/free-brands-svg-icons';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';


@NgModule({
  declarations: [
    AuthComponent, 
    SignupComponent, 
    LoginComponent],
  imports: [
    CommonModule,
    AuthRoutingModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatDividerModule,
    MatInputModule,
    MatButtonModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatDividerModule,
    MatInputModule,
    MatButtonModule,
    FlexLayoutModule,
    FontAwesomeModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule
    ],
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: ConfirmedValidator,
    multi: true
  }],
})
export class AuthModule {
  //Fix later for Google button
  // library.add(faGoogle, faLinkedin, faLinkedinIn);
 }
