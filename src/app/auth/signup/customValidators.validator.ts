import { isNull } from '@angular/compiler/src/output/output_ast';
import { FormGroup, ValidatorFn, AbstractControl } from '@angular/forms';

/** A hero's name can't match the given regular expression */
export function ConfirmedValidator(passwordControl : string): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {

        const confirmPassword = control.value;
        const password = control.root.get(passwordControl)?.value;

        const passCheck = (password !== confirmPassword);
        return passCheck ? {confirmedValidator: true } : null;

    };
  }
