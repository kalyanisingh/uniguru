import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';

import { UserService } from '../../services/user/user.service';

import { User } from '../../models/user.model';

import { ErrorStateMatcher } from '@angular/material/core';
import { ConfirmedValidator } from './customValidators.validator';

import { LocalAuthService } from 'src/app/services/auth/local-auth.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';




@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  

  minPasswordLength: number = 8;

  public signupForm: FormGroup = new FormGroup({
    fullName: new FormControl(null, Validators.required),
    email: new FormControl(null, [
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$')
    ]),
    password: new FormControl(null, [Validators.required, Validators.minLength(this.minPasswordLength)]),
    confirmPassword: new FormControl(null, [Validators.required, ConfirmedValidator('password')] ),
    birthDate: new FormControl(null, Validators.required),
    cityState: new FormControl(null),
    country: new FormControl(null)
  } 

  );

  matcher = new MyErrorStateMatcher();
  
  
  countries: String[] = [ 'Afghanistan','Albania','Algeria','American Samoa','Andorra','Angola','Anguilla','Antarctica',
  'Antigua and Barbuda','Argentina','Armenia','Aruba','Australia','Austria','Azerbaijan','Bahamas','Bahrain',
  'Bangladesh','Barbados','Belarus','Belgium','Belize','Benin','Bermuda','Bhutan','Bolivia',
  'Bosnia and Herzegovina','Botswana','Bouvet Island','Brazil','British Indian Ocean Territory',
  'British Virgin Islands','Brunei','Bulgaria','Burkina Faso','Burma','Burundi','Cambodia','Cameroon','Canada',
  'Cape Verde','Cayman Islands','Central African Republic','Chad','Chile','China','Christmas Island',
  'Cocos (Keeling) Islands','Colombia','Comoros','Congo, Democratic Republic of', 'Congo, Republic of the',
  'Cook Islands','Costa Rica','Cote d\'Ivoire','Croatia','Cuba','Curacao','Cyprus','Czech Republic',
  'Denmark','Djibouti','Dominica','Dominican Republic','Ecuador','Egypt','El Salvador','Equatorial Guinea',
  'Eritrea','Estonia','Ethiopia','Falkland Islands (Islas Malvinas)','Faroe Islands','Fiji',
  'Finland','France','France, Metropolitan','French Guiana','French Polynesia','French Southern and Antarctic Lands',
  'Gabon','Gambia', 'Gaza Strip','Georgia','Germany','Ghana','Gibraltar','Greece','Greenland','Grenada','Guadeloupe',
  'Guam','Guatemala','Guernsey','Guinea','Guinea-Bissau','Guyana','Haiti','Heard Island and McDonald Islands',
  'Holy See (Vatican City)','Honduras','Hong Kong (SAR China)','Hungary','Iceland','India','Indonesia','Iran','Iraq',
  'Ireland','Isle of Man','Israel','Italy','Jamaica','Japan','Jersey','Jordan','Kazakhstan','Kenya','Kiribati','Korea, South',
  'Kosovo','Kuwait','Kyrgyzstan','Laos','Latvia','Lebanon','Lesotho','Liberia','Libya','Liechtenstein','Lithuania',
  'Luxembourg','Macau (SAR China)','Macedonia','Madagascar','Malawi','Malaysia','Maldives','Mali','Malta',
  'Marshall Islands','Martinique','Mauritania','Mauritius','Mayotte','Mexico','Micronesia, Federated States of',
  'Moldova','Monaco','Mongolia','Montenegro','Montserrat','Morocco','Mozambique','Namibia','Nauru','Nepal',
  'Netherlands','New Caledonia','New Zealand','Nicaragua','Niger','Nigeria','Niue','Norfolk Island',
  'Northern Mariana Islands','Norway','Oman','Pakistan','Palau','Panama','Papua New Guinea','Paraguay','Peru',
  'Philippines','Pitcairn Islands','Poland','Portugal','Puerto Rico','Qatar','Reunion','Romania','Russia','Rwanda',
  'Saint Barthelemy','Saint Helena, Ascension, and Tristan da Cunha','Saint Kitts and Nevis','Saint Lucia','Saint Martin',
  'Saint Pierre and Miquelon','Saint Vincent and the Grenadines','Samoa','San Marino','Sao Tome and Principe',
  'Saudi Arabia','Senegal','Serbia','Seychelles','Sierra Leone','Singapore','Sint Maarten','Slovakia','Slovenia',
  'Solomon Islands','Somalia','South Africa','South Georgia and the Islands','South Sudan','Spain','Sri Lanka','Sudan',
  'Suriname','Svalbard','Swaziland','Sweden','Switzerland','Syria','Taiwan,Province of China',
  'Tajikistan','Tanzania','Thailand','Timor-Leste','Togo','Tokelau','Tonga','Trinidad and Tobago',
  'Tunisia','Turkey','Turkmenistan','Turks and Caicos Islands','Tuvalu','Uganda','Ukraine','United Arab Emirates',
  'United Kingdom','United States','United States Minor Outlying Islands','Uruguay','Uzbekistan','Vanuatu','Venezuela',
  'Vietnam','Virgin Islands','Wallis and Futuna','West Bank','Western Sahara','Yemen','Zambia','Zimbabwe' ];

  


  constructor(private UserService: UserService,
              private LocalAuthService: LocalAuthService,
              private http: HttpClient
              ) {   }


  ngOnInit() { }


  get f(){

    return this.signupForm?.controls;

  }

  onSubmit() {

    const birthDate = this.signupForm.value.birthDate.toDateString();


    const user = new User(
        this.signupForm.value.email,
        this.signupForm.value.password,
        this.signupForm.value.fullName,
        birthDate,
        this.signupForm.value.cityState,
        this.signupForm.value.country,
        "Local",
        ""
    );


  // this.http.get(this.LocalAuthService.signUpWithEmailPassword(user), { observe: 'response' })
  // .subscribe(response => console.log(response.status));

    //var localAuthResponse : HttpResponse<string> ;
    //create a user in FireBase authentication
    console.log(user);
    this.LocalAuthService.signUpWithEmailPassword(user);
    // (this.LocalAuthService.signUpWithEmailPassword(user))
    // .subscribe( result => result.json());

    //create a user in Atlab DB backend 
     this.UserService.createUser(user);

     //empty out the signup form once done
     this.signupForm.reset();

  }

};


/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
   }

}

