import { Component, OnInit, EventEmitter,Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/models/user.model';
import { HeaderService } from 'src/app/services/header/header.service';
import { GoogleAuthService } from 'src/app/services/auth/google-auth.service';
// import * as EventEmitter from 'events';
import * as firebase from 'firebase';
import { Observable } from 'rxjs';
import { LocalAuthService } from 'src/app/services/auth/local-auth.service';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm!: FormGroup;

  
  user$: Observable<firebase.default.User>;

  // Event Emitter to pass click event to app-component
  @Output() onHomeClicked = new EventEmitter<boolean>();
  @Output() onPlanClicked = new EventEmitter<boolean>();
  @Output() onContactusClicked = new EventEmitter<boolean>();
  @Output() onAboutusClicked = new EventEmitter<boolean>();

  constructor(
    private headerService: HeaderService,
    private googleAuth: GoogleAuthService,
    private localAuthService: LocalAuthService
  ) { this.user$ = this.googleAuth.user; }

  onSubmit() {
    // const user = new User(S
    //   this.loginForm.value.email,
    //   this.loginForm.value.passwordSSS
    // );
    
   this.localAuthService.signInWithEmailPassword(this.loginForm.value.email,
                                                 this.loginForm.value.password );

   this
  }


  loginWithGoogle() {
    this.googleAuth.signIn();
  }

  logoutofGoogle() {
    this.googleAuth.signOut();
  }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      email: new FormControl(null, [
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$')
      ]),
      password: new FormControl(null, Validators.required),
    })
  }

}
