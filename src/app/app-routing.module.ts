import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import { HomeComponent } from './home/home.component';
import { PlansComponent } from './home/plans/plans.component';
//import { DashboardComponent } from './dashboard/dashboard.component';
import { PaymentComponent } from './payment/payment.component';
//import { AdminComponent } from './admin/admin.component';
// import { PlansdetailComponent } from './home/plans/plansdetail/plansdetail.component';
import { DashboardComponent } from './dashboard/dashboard.component';
// import { AuthComponent } from './auth/auth.component';
// import { PaymentComponent } from './payment/payment.component';
// import { AdminComponent } from './admin/admin.component';


const APP_ROUTES: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'home/plans', component: PlansComponent,
      loadChildren: './home/plans/plansdetail/plansdetail.module#PlansdetailModule'},
  // { path: 'home/plans', component: PlansdetailComponent,
  //     loadChildren: './home/plans/plansdetail/plansdetail.module#PlansdetailModule'},
  { path: 'home/dashboard', component: DashboardComponent, loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule) },
  // { path: 'home/dashboard', component: DashboardComponent,
  //     loadChildren: './dashboard/dashboard.module#DashboardModule'},
  { path: 'auth', component: AuthComponent,
      loadChildren: './auth/auth.module#AuthModule'},
  { path: 'payment', component: PaymentComponent,
      loadChildren: () => import( './payment/payment.module')
                          .then(any => any.PaymentModule)},
                          
  // { path: 'admin', component: AdminComponent,
  //     loadChildren: () => import( './admin/admin.module')
  //                         .then(any => any.AdminModule)  }    
];

@NgModule({
  imports: [ RouterModule.forRoot(
    APP_ROUTES, { preloadingStrategy: PreloadAllModules }
  )],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
