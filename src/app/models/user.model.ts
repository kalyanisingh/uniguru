export class User {
    constructor(
        public email: string,
        public password: string,
        public fullName?: string,
        public birthDate?: Date,
        public cityState?: string,
        public country?: string,
        public authType?: string,
        public profilePicture?: string 
    ) {}
}