import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlatinumComponent } from './platinum/platinum.component';
import { GoldComponent } from './gold/gold.component';
import { SilverComponent } from './silver/silver.component';
import { PaymentComponent } from './payment.component';
import { PaymentRoutingModule } from './payment-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    PaymentRoutingModule,
    ReactiveFormsModule,
    RouterModule
  ],
  declarations: [PlatinumComponent, GoldComponent, SilverComponent, PaymentComponent]
})
export class PaymentModule { }
