import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { PaymentService } from '../../services/payment/payment.service';

@Component({
  selector: 'app-gold',
  templateUrl: './gold.component.html',
  styleUrls: ['./gold.component.scss']
})
export class GoldComponent implements OnInit {
  
  goldPaymentForm: FormGroup | undefined;  

  constructor(
    private paymentService: PaymentService
  ) { }

  onSubmit() {      
    // var handler = StripeCheckout.configure({
    //   key: 'pk_test_xDCGZVisgFuPsb53DjITLNi7',
    //   image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
    //   locale: 'auto',
    //   token: token => {
    //     this.paymentService.sendToken(token)
    //     .subscribe(
    //       data => console.log(data),
    //       error => console.error(error)
    //     );
    //   }        
    // });

    // document.getElementById('payButton').addEventListener('click', function(e) {
    //   // Open Checkout with further options:
    //   handler.open({
    //     name: 'Uniguru',
    //     description: 'Test',
    //     amount: 2000,
    //     zipCode: true
    //   });
    //   e.preventDefault();
    // });

    // // Close Checkout on page navigation:
    // window.addEventListener('popstate', function() {
    //   handler.close();
    // });

  }

  ngOnInit() {
    this.goldPaymentForm = new FormGroup({
      card: new FormControl(null),
      expirationMonth: new FormControl(null),
      expirationYear: new FormControl(null),
      cvc: new FormControl(null)
    })
  }

  // sendTokenToService(token) {
  //   this.paymentService.sendToken(token)
  //         .subscribe(
  //           data => console.log(data),
  //           error => console.error(error)
  //       );
  // }

}
