import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { SilverComponent } from './silver/silver.component';
import { GoldComponent } from './gold/gold.component';
import { PlatinumComponent } from './platinum/platinum.component';

const PAYMENT_ROUTES: Routes = [
  { path: '', redirectTo: 'silver', pathMatch: 'full'},
  { path: 'silver', component: SilverComponent },
  { path: 'gold', component: GoldComponent },
  { path: 'platinum', component: PlatinumComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PAYMENT_ROUTES)
  ],
  declarations: [],
  exports: [RouterModule]
})
export class PaymentRoutingModule { }
