// import { Injectable } from '@angular/core';
// import { Http, Headers, Response } from '@angular/http';
// import { HttpParams, HttpClient } from '@angular/common/http';
// import 'rxjs';
// import { Observable, BehaviorSubject } from 'rxjs';
// import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
// import { UserProfile } from '../../models/profile.model';
// import { SuggestedUniversities } from '../admin/sugguni.model';

// @Injectable()
// export class AdminService implements Resolve<any>{

//   private userDataSource = new BehaviorSubject<any>('default message');
//   public userProfileId: any;
//   currentUserData = this.userDataSource.asObservable();

//   constructor(
//     private http: Http,
//     private httpClient: HttpClient,
//     private router: Router
//     ) { }

//   // Get all users from backend
//   getAllUsers(){
//     return this.http.get('http://localhost:3000/admin')
//     .map((response : Response) => response.json())
//     .catch((error : Response) => Observable.throw(error.json())); 
//   }

//   resolve(route: ActivatedRouteSnapshot, rstate: RouterStateSnapshot): Observable<any> {
//     // Get updated value from the behavior subject var
//     this.currentUserData.subscribe(userData => {
//       this.userProfileId = userData._id;
//     });
//     // Get user profile of the user
//     return this.http.get('http://localhost:3000/admin/user/' + this.userProfileId)
//     .map((response : Response) => response.json())
//     .catch((error : Response) => Observable.throw(error.json())); 
//   } 

//   updateUserProfile(id: String, userProfile: any){
//     return this.http.put('http://localhost:3000/admin/userprofile/' + id, userProfile)
//         .map((response : Response) => response.json())
//         .catch((error : Response) => Observable.throw(error.json()));     
//   }

//   deleteUserProfile(id: String){
//     return this.http.delete('http://localhost:3000/admin/userprofile/' + id)
//       .map((response : Response) => response.json())
//       .catch((error : Response) => Observable.throw(error.json())); 
//   }

//   redirectToUserProfile(userData: any) {
//     // Update the behavior subject
//     this.userDataSource.next(userData);
//     this.router.navigateByUrl('admin/userinfo');    
//   }

//   redirectToSuggestedUniversities(userData: any) {
//     // Update the behavior subject
//     this.userDataSource.next(userData);
//     // Navigate to suggested universities page 
//     this.router.navigateByUrl('admin/suggesteduniversity');    
//   }

//   saveUniversityInfo(suggestedUniversities: SuggestedUniversities, id: String){
//     const body = JSON.stringify(suggestedUniversities);
//     const headers = new Headers({'Content-Type': 'application/json'});
//     return this.http.post('http://localhost:3000/admin/suggested/' + id, body, {headers : headers})
//         .map((response : Response) => response.json())
//         .catch((error : Response) => Observable.throw(error.json()));     
//   }

//   updateSuggestedUniversity(suggestedUniversities: SuggestedUniversities, id: String, idx: String){
//     const body = JSON.stringify(suggestedUniversities);
//     const headers = new Headers({'Content-Type': 'application/json'});
//     return this.http.put('http://localhost:3000/admin/suggested/' + id + '/' + idx, body, {headers : headers})
//         .map((response : Response) => response.json())
//         .catch((error : Response) => Observable.throw(error.json()));     
//   }

//   deleteSuggestedUniversity(id: String, idx: String) {
//     return this.http.delete('http://localhost:3000/admin/suggested/'+ id + '/index/' + idx)
//     .map((response : Response) => response.json())
//     .catch((error : Response) => Observable.throw(error.json()));
//   }

//   getSuggestedUniversities(id: String) {
//     return this.http.get('http://localhost:3000/admin/suggested/' + id)
//     .map((response : Response) => response.json())
//     .catch((error : Response) => Observable.throw(error.json())); 
//   }

// }
