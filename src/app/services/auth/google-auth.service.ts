import { Injectable, NgZone } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { environment } from 'src/environments/environment';
import 'firebase/firestore';
import { Observable, ReplaySubject } from 'rxjs';
import { HeaderService } from '../header/header.service';
import { UserService } from '../user/user.service';
import * as firebase from 'firebase';
import { User } from 'src/app/models/user.model';


declare var gapi: any;


@Injectable({
  providedIn: 'root'
})
export class GoogleAuthService {
  private isLoggedIn$ = new ReplaySubject<boolean>(1);
  private user$ = new ReplaySubject<firebase.default.User>(1);
  private localUser! : User;


  constructor(public afAuth: AngularFireAuth, 
              public HeaderService: HeaderService, 
              public UserService: UserService,
              private ngZone: NgZone) {
    this.isLoggedIn$.next(false);
    this.afAuth.onAuthStateChanged(async (user: any) => {
      this.ngZone.run(() => {
        this.isLoggedIn$.next(Boolean(user));
        this.user$.next(user);
      })
    });

   
  }

  get isLoggedIn(): Observable<boolean> {
    return this.isLoggedIn$.asObservable();
  }

  get user(): Observable<firebase.default.User> {    
    return this.user$.asObservable();
  }

  async signOut(): Promise<void> {
    return firebase.default.auth().signOut();
  }

  private async initAuth2(baseScopes: string[]): Promise<void> {
    await this.HeaderService.initClient(baseScopes);
    if (!gapi.auth2.getAuthInstance()) {
      gapi.auth2.init({
        client_id: environment.firebase.clientID,
        scope: baseScopes.join(' ')
      });
    }
  }

  async signIn(baseScopes: string[] = ['email']): Promise<void> {
    await this.initAuth2(baseScopes);
    const googleUser = await gapi.auth2.getAuthInstance().signIn({
      prompt: 'select_account'
    });
    const token = googleUser.getAuthResponse().id_token;
    const credential = firebase.default.auth.GoogleAuthProvider.credential(token);
    await firebase.default.auth().signInWithCredential(credential);

    
       // saving the Google User to backend

       // creating the user object from Google Info
       this.user$.subscribe({
        next: currentUser => { 

          // console.log('User inside Firebase Sign In : ' +currentUser);
          const name = (currentUser.displayName) || undefined;
          // const firstLastName = name?.split(" ");
          // const firstName = firstLastName![0];
          // const lastName = firstLastName![1];

         
          this.localUser = {
            fullName : name,
            email : currentUser.email as string,
            password : '',
            birthDate: new Date("01/01/2099"),
            cityState: "",
            country: "",
            authType : "Google",
            profilePicture : currentUser.photoURL as string
           };

        },
        error: err => console.error('Observer got an error: ' + err),
      })

      //console.log(this.localUser);

      // Saving the Google User to Atlas backend
      this.UserService.createUser(this.localUser);
      

  }
}
