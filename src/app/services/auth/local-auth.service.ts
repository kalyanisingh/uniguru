import { HttpResponseBase } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import "firebase/auth";
import { Observable, Subject } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { HttpResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})


export class LocalAuthService {
  
 // private currentUser = firebase.default.auth().currentUser;

constructor() { }
     
signInWithEmailPassword(email : string, password : string) {
  
   firebase.default.auth().signInWithEmailAndPassword(email, password)
    .then((userCredential) => {
      // Signed in
      var user = userCredential.user;
      console.log('User inside Firebase Auth : ' + user);
    })
    .catch((error) => {
      var errorCode = error.code;
      var errorMessage = error.message;

      console.log(errorMessage);
    });
  // [END auth_signin_password]
  
}


signUpWithEmailPassword(user : User) {

  var email = user.email;
  var password = user.password || "";

  // [START auth_signup_password]
  firebase.default.auth().createUserWithEmailAndPassword(email, password)
    .then((userCredential) => {
      // Signed in 
      var localFirebaseUser = userCredential.user;

     // console.log("1 " +localFirebaseUser);
      localFirebaseUser?.updateProfile({
          displayName: user.fullName,
         
        }).then(function() {
          console.log("Firebase user updated successfully");
          console.log(localFirebaseUser);
        }).catch(function(error) {
          console.log("Error occured in updating Firebase user");
        });
      
     // console.log(user);
    })
    .catch((error) => {
      var errorCode = error.code;
      var errorMessage = error.message;
    });
  // [END auth_signup_password]

}

// function sendEmailVerification() {
//   // [START auth_send_email_verification]
//   firebase.auth().currentUser.sendEmailVerification()
//     .then(() => {
//       // Email verification sent!
//       // ...
//     });
//   // [END auth_send_email_verification]
// }

// function sendPasswordReset() {
//   const email = "sam@example.com";
//   // [START auth_send_password_reset]
//   firebase.auth().sendPasswordResetEmail(email)
//     .then(() => {
//       // Password reset email sent!
//       // ..
//     })
//     .catch((error) => {
//       var errorCode = error.code;
//       var errorMessage = error.message;
//       // ..
//     });
//   // [END auth_send_password_reset]
// }

}
