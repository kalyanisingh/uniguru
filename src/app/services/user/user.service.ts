import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import 'rxjs';
import { Observable, BehaviorSubject } from 'rxjs';
import { User } from '../../models/user.model';
import { catchError, map } from 'rxjs/operators';

@Injectable()
export class UserService {
  
  private userLoginDataSource = new BehaviorSubject<any>('default message');
  currentUserData = this.userLoginDataSource.asObservable();

  private routeURI = 'http://localhost:3000/user/';

  constructor(private http: HttpClient) { }

  createUser(user: User) { /// was signup
    const body = JSON.stringify(user);
    console.log(body);
    
    const headers = new HttpHeaders({'Content-Type': 'application/json'});

    this.http.post<any>(this.routeURI, body, {headers : headers}).subscribe(        
      data => console.log(data),
      err => console.log(err)      
    )      
  }


  updateUser(user : User)
  {

  }

  // login(user: User) {
  //   const body = JSON.stringify(user);
  //   const headers = new Headers({'Content-Type': 'application/json'});
    
  //   return this.http.post('http://localhost:3000/user/login', body, {headers: headers})
  //     .map((response: Response) => {
  //       response.json();
  //       this.userLoginDataSource.next( response.json() );
  //       //console.log(this.userLoginDataSource);
  //       //console.log(this.currentUserData);
  //       })
  //     .catch((error: Response) => Observable.throw(error.json()));
  // }

  // logout() {
  //   return this.http.get(`http://localhost:3000/user/logout`)
  //     .map((response: Response) => response.json())
  //     .catch((error: Response) => Observable.throw(error.json()));

  // }

  getCurrentUser(emailId: string) {
    let params = new HttpParams().set("email",emailId)
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    const opts = {
      headers: headers,
      params: params
    }
    return this.http.get(this.routeURI, opts);
  };
}
