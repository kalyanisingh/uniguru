import { Injectable, NgZone } from '@angular/core';
import { Observable, Observer, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';



declare var gapi: any;

@Injectable({
    providedIn: 'root'
  })


export class HeaderService {

    constructor() { }

    clicked: boolean | undefined;
    // Observable sources
    private homeClickSource = new Subject<boolean>();
    private contactusClickSource = new Subject<boolean>();
    private aboutusClickSource = new Subject<boolean>();
    private homeFlagSource = new Subject<boolean>();
    private planClickSource = new Subject<boolean>();
    


    setPlanClicked(clicked: boolean) {
        console.log(clicked);
        this.planClickSource.next(clicked);
    }
    setHomeClicked(clicked: boolean) {
        this.homeClickSource.next(clicked);
    }
    setAboutusClicked(clicked: boolean) {
        console.log(clicked);
        this.aboutusClickSource.next(clicked);
    }
    setContactusClicked(clicked: boolean) {
        this.contactusClickSource.next(clicked);
    }
    setHomeFlag(homeFlag: boolean) {
        this.homeFlagSource.next(homeFlag);
    }

    // provide value to home component
    getPlanClicked(): Observable<boolean> {
        return this.planClickSource.asObservable();
    }
    getHomeClick(): Observable<boolean> {
        return this.homeClickSource.asObservable();
    }
    getAboutusClick(): Observable<boolean> {
        return this.aboutusClickSource.asObservable();
    }
    getContactusClick(): Observable<boolean> {
        return this.contactusClickSource.asObservable();
    }
    getHomeFlag(): Observable<boolean> {
        return this.homeFlagSource;
    }



  private readonly url: string = 'https://apis.google.com/js/api.js';
  private loaded = false;

  private loadGapi(): Observable<void> {
    return new Observable((observer: Observer<void>) => {
      if (this.loaded) {
        observer.next();
        observer.complete();
        return;
      }
      const node = document.createElement('script');
      node.src = this.url;
      node.type = 'text/javascript';
      document.getElementsByTagName('head')[0].appendChild(node);
      node.onload = () => {
        this.loaded = true;
        observer.next();
        observer.complete();
      };
      node.onerror = (error) => {
        observer.error(error);
      };
    });
  }

  public async initClient(baseScopes: string[] = ['email']) {
    await this.loadGapi().toPromise();
    return new Promise<void>(async (resolve, reject) => {
      try {
        const initClient = async (error: any) => {
          if (error) {
            return reject(error);
          }
          try {
            await gapi.client.init({
              apiKey: environment.firebase.apiKey,
              clientId: environment.firebase.clientID,
              scope: baseScopes.join(' ')
            });
            return resolve();
          } catch (error) {
            return reject(error);
          }
        }
        gapi.load('client:auth2', initClient);
      } catch (error) {
        return reject(error);
      }
    });
  }
}
