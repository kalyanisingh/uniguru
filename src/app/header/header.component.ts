import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HeaderService } from '../services/header/header.service';
import { GoogleAuthService } from '../services/auth/google-auth.service';
// import * as EventEmitter from 'events';
import * as firebase from 'firebase';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  clickedPlan: boolean = false;
  clickedAboutUs: boolean = false;
  clickedContactUs: boolean = false;
  clickedHome : boolean = false;
  homeFlag: boolean = false;
  loginFlag: boolean = false;
  adminFlag: boolean = false;

  user$!: Observable<firebase.default.User>;
  defaultProfilePhoto = '/src/assets/images/default_user_avatar.png';

  // Event Emitter to pass click event to app-component
  @Output() onHomeClicked = new EventEmitter<boolean>();
  @Output() onPlanClicked = new EventEmitter<boolean>();
  @Output() onContactusClicked = new EventEmitter<boolean>();
  @Output() onAboutusClicked = new EventEmitter<boolean>();

  constructor(
    private headerService: HeaderService,
    private googleAuth: GoogleAuthService
  ) { 
    this.user$ = this.googleAuth.user; 
   }

  onHome(clicked: boolean) {
    this.headerService.setHomeClicked(clicked);   // emit event to app component
  }
  onPlan(clicked: boolean){
    this.headerService.setPlanClicked(clicked);
  }
  onContactus(clicked: boolean) {
    this.headerService.setContactusClicked(clicked);   // emit event to app component
  }
  onAboutus(clicked: boolean) {
    this.headerService.setAboutusClicked(clicked);   // emit event to app component
  }

  login() {
    this.googleAuth.signIn();
  }

  logout() {
    this.googleAuth.signOut();
  }

  ngOnInit(): void {
   this.user$ = this.googleAuth.user; 
    this.user$.subscribe({
        next: x => console.log('Observer got a next value: ' + x.metadata),
        error: err => console.error('Observer got an error: ' + err),
    })
  }

}
