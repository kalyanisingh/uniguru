import { Component } from '@angular/core';
import { HeaderService } from './services/header/header.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  clicked: boolean | undefined;
  // Inject Header service to get click event from header component and pass it to home component
  constructor(
      private headerService: HeaderService,
    ) {}

}
