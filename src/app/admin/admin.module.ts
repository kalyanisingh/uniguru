import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { UsersComponent } from './users/users.component';
import { AdminRoutingModule } from './admin-routing.module';
import { SuiModule } from 'ng2-semantic-ui';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule, MatInputModule, MatSelectModule, MatSortModule, MatPaginatorModule } from '@angular/material';
import { UserprofileComponent } from './userprofile/userprofile.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatSidenavModule } from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import { SuggesteduniComponent } from './suggesteduni/suggesteduni.component'; 
import { FlexLayoutModule } from '@angular/flex-layout';
import {MatExpansionModule} from '@angular/material/expansion';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faUserCircle, faLayerGroup } from '@fortawesome/free-solid-svg-icons';

@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    SuiModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatSidenavModule,
    MatListModule,
    FlexLayoutModule,
    MatExpansionModule,
    FontAwesomeModule,
    MatSortModule,
    MatPaginatorModule
  ],
  declarations: [AdminComponent, UsersComponent, UserprofileComponent, SuggesteduniComponent],
  exports: [
    MatTableModule,
    MatSortModule,
    MatPaginatorModule
  ]
})
export class AdminModule { 
  constructor() {
    library.add(faUserCircle, faLayerGroup);
  }
}
