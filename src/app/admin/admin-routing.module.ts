import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './users/users.component';
import { UserprofileComponent } from './userprofile/userprofile.component';
import { AdminService } from '../services/admin/admin.service';
import { SuggestedComponent } from '../dashboard/suggested/suggested.component';
import { SuggesteduniComponent } from './suggesteduni/suggesteduni.component';

const ADMIN_ROUTES: Routes = [
  { path: '', redirectTo: 'users', pathMatch: 'full' },
  { path: 'users', component: UsersComponent },
  { path: 'userinfo', component: UserprofileComponent, resolve: {userProfileData: AdminService} },
  { path: 'suggesteduniversity', component: SuggesteduniComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ADMIN_ROUTES)
  ],
  exports: [RouterModule],
  providers: [AdminService]
})
export class AdminRoutingModule { }
