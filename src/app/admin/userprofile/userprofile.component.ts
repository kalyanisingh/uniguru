import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../services/admin/admin.service';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { UserProfile } from '../../models/profile.model';

@Component({
  selector: 'app-userprofile',
  templateUrl: './userprofile.component.html',
  styleUrls: ['./userprofile.component.scss']
})
export class UserprofileComponent implements OnInit {
  userData: any;
  public userProfileData: any;
  profileForm: FormGroup;
  public userResumePath: String;
  public updateFlag: Boolean;
  public editIndex  : any;
  public userProfileId: String;

  public universities: { 
    degree: String,
    major: String,
    name: String,
    gpa: String }[] = new Array();
  
  public greInfo : {
    quant : Number,
    verbal : Number,
    awa    : Number,
    overallGreScore : Number
  };

  public englishlangtestInfo :{
    testType : String,
    reading : Number,
    writing : Number,
    speaking : Number,
    listening : Number,
    overallLangScore : Number
  };

  constructor(
    private adminService: AdminService,
    private activatedRoute: ActivatedRoute
    ) { }

  ngOnInit() {
    this.profileForm = new FormGroup({
      universityInfo: new FormGroup({
        degree: new FormControl(''),
        major: new FormControl(''),
        name: new FormControl(''),
        gpa: new FormControl('')
      }),

      greInfo : new FormGroup({
        quant: new FormControl(''),
        verbal: new FormControl(''),
        awa: new FormControl(''),
        overallGreScore :  new FormControl('')
      }),

      englishlangtestInfo : new FormGroup({
        testType: new FormControl(''),
        reading: new FormControl(''),
        writing: new FormControl(''),
        speaking: new FormControl(''),
        listening: new FormControl(''),
        overallLangScore: new FormControl('')
      })
    });

    // Get data from users components
    this.adminService.currentUserData.subscribe(userData => this.userData = userData);

    // First resolve then load the page
    this.activatedRoute.data.map(data => data.userProfileData)
      .subscribe(
        data => {
        this.userProfileData = data;
        this.parseUserData(data.userProfile);
        this.setFormData();
      },
      error => console.error(error)
      );

    // Disable form editing
    this.profileForm.disable();
  }

  onEdit() {
    this.profileForm.get('englishlangtestInfo').enable();
  }

  addUniversity() {
    const degree = this.profileForm.value.universityInfo.degree;
    const major = this.profileForm.value.universityInfo.major;
    const name = this.profileForm.value.universityInfo.name;
    const gpa = this.profileForm.value.universityInfo.gpa;

    var singleuniinfo = {degree,
                         major,
                         name,
                         gpa
                        };
  
    this.universities.push(singleuniinfo);
    console.log(this.universities);    
  }

  editUniversity(university) { 
    
    console.log(university);

    this.profileForm.patchValue({
      universityInfo: {
        degree : university.degree,
        major : university.major,
        name: university.name,
        gpa: university.gpa
      }      
    });
  
    console.log('I am here too!');

    for(let i = 0 ;i <= this.universities.length ; i++){
      if(university == this.universities[i]){
        this.editIndex = i;
      }
     }
    
    this.profileForm.get('universityInfo').enable();

    this.updateFlag = true;
  }

  removeUniversity(university) {
      for(let i=0 ;i<= this.universities.length ;i++){
       if(university == this.universities[i]){
        this.universities.splice(i,1)
       }
      }
      console.log(this.universities);
  }

  cancelUniversity(university) {    
    this.profileForm.setValue({
      universityInfo: {
        degree : '',
        major : '',
        name: '',
        gpa: ''
      }      
    });
    this.updateFlag = true;
}

  updateUniversity(university) {
    const degree = this.profileForm.value.universityInfo.degree;
    const major = this.profileForm.value.universityInfo.major;
    const name = this.profileForm.value.universityInfo.name;
    const gpa = this.profileForm.value.universityInfo.gpa;

    var singleuniinfo = {degree,
                         major,
                         name,
                         gpa
                        };

    //console.log(singleuniinfo); 
    this.universities.splice(this.editIndex, 1, singleuniinfo);    
    this.updateFlag = false;
  }

  onUpdate(){
    const quant = this.profileForm.value.greInfo.quant;
    const verbal = this.profileForm.value.greInfo.verbal;
    const awa = this.profileForm.value.greInfo.awa;
    const overallGreScore = this.profileForm.value.greInfo.overallGreScore;

    this.greInfo = { quant,
                     verbal,
                     awa,
                     overallGreScore
                   };

    const testType = this.profileForm.value.englishlangtestInfo.testType;
    const reading = this.profileForm.value.englishlangtestInfo.reading;
    const listening = this.profileForm.value.englishlangtestInfo.listening;
    const writing = this.profileForm.value.englishlangtestInfo.writing;
    const speaking = this.profileForm.value.englishlangtestInfo.speaking;
    const overallLangScore = this.profileForm.value.englishlangtestInfo.overallLangScore;

    this.englishlangtestInfo = {
      testType,
      reading,
      writing,
      speaking,
      listening,
      overallLangScore
    };

    this.userResumePath = this.userProfileData.resumeFilePath;
    this.userProfileId = this.userProfileData._id;

    const userProfile = new UserProfile (
      this.universities,
      this.greInfo,
      this.englishlangtestInfo,
      this.userResumePath,   
    );

    // console.log(userProfile);
    
    this.adminService.updateUserProfile(this.userProfileData._id, userProfile)
      .subscribe(
        data => console.log(data),
        error => console.error(error)
    ); 

  }

  onDelete() {
    this.adminService.deleteUserProfile(this.userProfileData._id)
    .subscribe(
      data => console.log(data),
      error => console.error(error)
  ); 
  }

  parseUserData(userProfileData) {
    // console.log(userProfileData);
    this.userProfileData = userProfileData;
    this.universities = this.userProfileData.universityInfo;
    console.log(this.userProfileData);
  }

  setFormData(){
    this.profileForm = new FormGroup({
      universityInfo: new FormGroup({
        degree: new FormControl(''),
        major: new FormControl(''),
        name: new FormControl(''),
        gpa: new FormControl('')
      }),

      greInfo : new FormGroup({
        quant: new FormControl(this.userProfileData.greInfo.quant),
        verbal: new FormControl(this.userProfileData.greInfo.verbal),
        awa: new FormControl(this.userProfileData.greInfo.awa),
        overallGreScore : new FormControl(this.userProfileData.greInfo.overallGreScore)
      }),

      englishlangtestInfo : new FormGroup({
        testType: new FormControl(this.userProfileData.englishlangtestInfo.testType),
        reading: new FormControl(this.userProfileData.englishlangtestInfo.reading),
        writing: new FormControl(this.userProfileData.englishlangtestInfo.writing),
        speaking: new FormControl(this.userProfileData.englishlangtestInfo.speaking),
        listening: new FormControl(this.userProfileData.englishlangtestInfo.listening),
        overallLangScore: new FormControl(this.userProfileData.englishlangtestInfo.overallLangScore)
      })
    });
  }

}
