import { Component, OnInit, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import { AdminService } from '../../services/admin/admin.service';
import { MatTableDataSource, MatSort, MatPaginator} from '@angular/material';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, AfterViewInit {
  displayedColumns = ['firstName', 'lastName', 'email', 'userProfile', 'suggestedUniversity'];
  dataSource: MatTableDataSource<any> = new MatTableDataSource([]);

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  userData: any;

  constructor(
    private adminService: AdminService,
    private router: Router
    ) { }  

  ngOnInit() {
    this.adminService.getAllUsers()
    .subscribe(
      data => {
        this.dataSource = new MatTableDataSource<any>(data.obj);
        // this.dataSource.sort = this.sort;
      },
      error => console.error(error)
    );
  }

  ngAfterViewInit() {
    this.adminService.getAllUsers()
    .subscribe(
      data => {
        this.dataSource = new MatTableDataSource<any>(data.obj);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      },
      error => console.error(error)
    );
  }

  // ngAfterViewInit() {
  //   console.log(this.dataSource);
  //   this.dataSource.sort = this.sort;
  // }

  onRowClicked(rowData) {
    // update the behavior subject var
    this.adminService.redirectToUserProfile(rowData);
  }

  onUserProfileClicked(user) {
    console.log(user);
    this.adminService.redirectToUserProfile(user);
  }

  onSuggUniClicked(user) { 
    this.adminService.redirectToSuggestedUniversities(user);
  }

}

