import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuggesteduniComponent } from './suggesteduni.component';

describe('SuggesteduniComponent', () => {
  let component: SuggesteduniComponent;
  let fixture: ComponentFixture<SuggesteduniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuggesteduniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuggesteduniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
