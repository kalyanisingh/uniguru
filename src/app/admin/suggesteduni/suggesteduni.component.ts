import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SuggestedUniversities } from '../../models/sugguni.model';
import { AdminService } from '../../services/admin/admin.service';

@Component({
  selector: 'app-suggesteduni',
  templateUrl: './suggesteduni.component.html',
  styleUrls: ['./suggesteduni.component.scss']
})
export class SuggesteduniComponent implements OnInit {
  suggestedUniversityForm: FormGroup;
  public sugguni = {suggestedUniversities: new Array()} as SuggUni;
  public suggestedUniversities = new Array();
  public usefulLinks = [];
  public currentUserID : String = null;
  public editIdx: String;
  public editID: String;
  public updateFlag: Boolean;

  constructor(private adminService: AdminService) { }

  onSubmit() {
    // Check if this is add or update request
    // Get the updated user data
    this.adminService.currentUserData.subscribe(userData => {
      this.currentUserID = userData._id;
    });
    // If this is add suggested university request
    if(!this.updateFlag) {
      this.usefulLinks = this.suggestedUniversityForm.value.usefulLinks.split(',');
      this.sugguni.suggestedUniversities.push(
        { name           : this.suggestedUniversityForm.value.name,
          universityType : this.suggestedUniversityForm.value.universityType,
          applicationDeadline : this.suggestedUniversityForm.value.applicationDeadline,
          applicationFee      : this.suggestedUniversityForm.value.applicationFee,
          gpaRequirement      : this.suggestedUniversityForm.value.gpaRequirement,
          GRERequirement      : this.suggestedUniversityForm.value.GRERequirement,
          englishLanguageRequirement  : this.suggestedUniversityForm.value.englishLanguageRequirement,
          applicationURL    :  this.suggestedUniversityForm.value.applicationURL,
          usefulLinks       :  this.usefulLinks
        });
      
      // Send request to the server
      this.adminService.saveUniversityInfo(this.sugguni, this.currentUserID)
      .subscribe(
             data => this.suggestedUniversities = data.obj.suggestedUniversities,
             error => console.error(error)
       );      
    } else {
      // If is is an update request
      // Check if useful links are updated?
      if(Array.isArray(this.suggestedUniversityForm.value.usefulLinks)) {
        this.usefulLinks = this.suggestedUniversityForm.value.usefulLinks;
      } else {
        this.usefulLinks = this.suggestedUniversityForm.value.usefulLinks.split(',');
      }
      
      this.sugguni.suggestedUniversities.push(
        { name           : this.suggestedUniversityForm.value.name,
          universityType : this.suggestedUniversityForm.value.universityType,
          applicationDeadline : this.suggestedUniversityForm.value.applicationDeadline,
          applicationFee      : this.suggestedUniversityForm.value.applicationFee,
          gpaRequirement      : this.suggestedUniversityForm.value.gpaRequirement,
          GRERequirement      : this.suggestedUniversityForm.value.GRERequirement,
          englishLanguageRequirement  : this.suggestedUniversityForm.value.englishLanguageRequirement,
          applicationURL    :  this.suggestedUniversityForm.value.applicationURL,
          usefulLinks       :  this.usefulLinks
        });
        // send update request to server
        this.adminService.updateSuggestedUniversity(this.sugguni, this.editID, this.editIdx)
        .subscribe(
          data => this.suggestedUniversities = data.obj.suggestedUniversities,
          error => console.error(error)
        );   
        this.suggestedUniversityForm.reset();  
        this.updateFlag = false;
    }
    
  }

  onCancel() {
    this.suggestedUniversityForm.reset();
    this.updateFlag = false;
  }

  onDelete(idx: String) {
    this.adminService.deleteSuggestedUniversity(this.editID, idx)
    .subscribe(
      data => this.suggestedUniversities = data.obj.suggestedUniversities,
      error => console.error(error)
    );   
  }

  ngOnInit() {
    this.adminService.currentUserData.subscribe(userData => {
      this.currentUserID = userData._id;
    });
    this.suggestedUniversityForm = new FormGroup({
      name           : new FormControl(null, Validators.required),
      universityType : new FormControl(null, Validators.required),
      applicationDeadline: new FormControl(null, Validators.required),
      applicationFee : new FormControl(null, Validators.required),
      gpaRequirement : new FormControl(null, Validators.required),
      GRERequirement : new FormControl(null, Validators.required),
      englishLanguageRequirement: new FormControl(null, Validators.required),
      applicationURL : new FormControl(null, Validators.required),
      usefulLinks    : new FormControl(null)
    });

    this.adminService.getSuggestedUniversities(this.currentUserID)
      .subscribe(
        data => {
          this.suggestedUniversities = data.obj.suggestedUniversities;
          this.editID = data.obj._id;
        },
        error => console.error(error)
      );
    }

    onEdit(suggested: any, idx: String) {
      this.editIdx = idx;
      this.updateFlag = true;
      this.suggestedUniversityForm = new FormGroup({
        name           : new FormControl(suggested.name, Validators.required),
        universityType : new FormControl(suggested.universityType, Validators.required),
        applicationDeadline: new FormControl(suggested.applicationDeadline, Validators.required),
        applicationFee : new FormControl(suggested.applicationFee, Validators.required),
        gpaRequirement : new FormControl(suggested.gpaRequirement, Validators.required),
        GRERequirement : new FormControl(suggested.GRERequirement, Validators.required),
        englishLanguageRequirement: new FormControl(suggested.englishLanguageRequirement, Validators.required),
        applicationURL : new FormControl(suggested.applicationURL, Validators.required),
        usefulLinks    : new FormControl(suggested.usefulLinks)
      });
  
    }

  }

export  interface SuggUni {
  suggestedUniversities: {
    name                 : String,
    universityType       : String
    applicationDeadline  : String,
    applicationFee       : String,
    gpaRequirement       : String,
    GRERequirement       : String,
    englishLanguageRequirement    : String,
    applicationURL       : String,
    usefulLinks          : String []
  } []
}
