// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyCQgkdakl230BN7RDa0sWfyAwwi9-bwPwU",
    authDomain: "uniguru-de58d.firebaseapp.com",
    projectId: "uniguru-de58d",
    storageBucket: "uniguru-de58d.appspot.com",
    messagingSenderId: "958180687839",
    appId: "1:958180687839:web:43d2cb9e0ecd472638ce6a",
    measurementId: "G-DWTQ0C2DQY",
    clientID: "958180687839-0h2u02iqvfgrrlm7j9e5ei5v157llmda.apps.googleusercontent.com"


  }
  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
